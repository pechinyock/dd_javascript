var timerLabel = document.getElementById('timerLable');
var boxQuote = document.getElementById('boxQuote');
var textQuote = document.getElementById('textQuote');
var quoteAuthor = document.getElementById('quoteAuthor');
var request = new XMLHttpRequest();

GetQuete();
boxQuote.addEventListener('click', GetQuete);

var secToRefresh = 10;
var timer = setTimeout(tick, 1000);


function tick()
{
    if(secToRefresh <= 1)
    {        
        GetQuete();
    }
    secToRefresh--;
    timerLabel.innerText = secToRefresh;
    timer = setTimeout(tick, 1000);
}

function GetQuete()
{    
    secToRefresh = 10;
    request.onload = onLoadHandler;
    
    request.open("GET", "https://favqs.com/api/qotd");
    request.send();
}

function onLoadHandler()
{
    var status = request.status;
    if(status == 200)
    {
        var currentQuote = JSON.parse(request.responseText);
        textQuote.innerText = currentQuote.quote.body;
        quoteAuthor.innerText = currentQuote.quote.author;        
    }
    else
    {
        alert('Got an error!');
    }
}
